const COMPLIANCE = require('./lib/compliance')
const CRM = require('./lib/crm')
const POS = require('./lib/pos')
const _API = require('./lib/cova')



class COVA {
    constructor(creds){
        this.pos = new POS(creds),
        this.crm = new CRM(creds),
        this.compliance = new COMPLIANCE(creds)
    }
}
module.exports = COVA;





