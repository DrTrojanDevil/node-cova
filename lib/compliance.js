const CovaAPI = require('./cova');
class COMPLIANCE extends CovaAPI {

    constructor(creds) {
        super(creds)
    }

    async customerDailyTHC(opts){
        const uri = `https://dispensary.iqmetrix.net/v1/companies/{${this.creds.CompanyId}}/locations/{${this.creds.LocationId}}/customer/{${opts.customerId}}/cannabispurchasesummary/today`;
        return this._makeCall('GET', uri)
    }

    // async getPurchasingLimits(opts){
    //     const uri = `https://dispensary.iqmetrix.net/v1/Companies/{${this.creds.CompanyId}}/Entities/EquivalencyPurchasingLimits`;
    //     return this._makeCall('GET', uri)
    // }

    async getPurchasingLimitsByLocation(opts){
        const uri = `https://dispensary.iqmetrix.net/Companies/{${this.creds.CompanyId}}/Entities(${this.creds.LocationId})/purchaselimits`;
        return this._makeCall('GET', uri)
    }

    async getCompanyLicenses(opts){
        const uri = `https://traceability.iqmetrix.net/v1/Companies/${this.creds.CompanyId}/Locations/${this.creds.LocationId}/Licences`;
        return this._makeCall('GET', uri)
    }

    async getSupplierLicenses(opts){
        const uri = `https://dispensary.iqmetrix.net/v1/Companies/${this.creds.CompanyId}/SupplierLicensing(${opts.supplierId})`;
        return this._makeCall('GET', uri)
    }




}

module.exports = COMPLIANCE;