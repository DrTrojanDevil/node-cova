const _ = require('lodash');
const request = require('request');
const rp = require('request-promise-native');

class CovaAPI {
    constructor(creds) {
        this.creds = creds;
        this._tokenExpireTime = 0;
        this.authHeader = {};
    }

    async _now() {
        return Math.round(new Date().getTime() / 1000);
    }
    
    async setEnitityId(entityId){
        this.creds.EntityId = entityId;
        return;
    }

    async setLocationId(locationId){
        this.creds.LocationId = locationId;
        return;
    }

    async getEnitityId(){
        return this.creds.EntityId;  
    }

    async getLocationId(){
        return this.creds.LocationId;
    }

    async  $getToken() {
        var options = {
            method: 'POST',
            uri: 'https://accounts.iqmetrix.net/v1/oauth2/token',
            form: this.creds,
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            }
        };
        const token = await rp(options)
        let t = JSON.parse(token);
        this.creds.access_token = t.access_token;
        this.authHeader = {
            'Authorization': `Bearer ${this.creds.access_token}`
        };
        let expireTime = this._now() + t.expires_in;
        this._tokenExpireTime = expireTime;

    }

    async $tokenIsFresh() {
        if (this._now() < this._tokenExpireTime) {
            return true;
        } else {
            await this.$getToken();
            return true;
        }
    }

    async _makeCall(method, uri, data, headers) {
        await this.$tokenIsFresh();
        let callHeders = this.authHeader;
        if (headers) {
            callHeders = _.extend(this.authHeader, headers);
        }
        const options = {
            method,
            uri,
            body: data,
            headers: callHeders
        };
        try {
            const result = JSON.parse(await rp(options));
            let res = _.omit(result, '_links')
            if (res._embedded && res._embedded.self) {
                return res._embedded.self
            } else {
                return res
            }
        } catch (err) {
            console.log(err)
        }
    }


}

module.exports = CovaAPI;