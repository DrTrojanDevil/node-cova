const CovaAPI = require('./cova');
class CRM extends CovaAPI {

    constructor(creds) {
        super(creds)
    }

    async createCustomer(opts){
        const uri = `https://crm.iqmetrix.net/v1/Companies(${opts.companyId})/CustomerFull`;
        return this._makeCall('POST', uri, opts.customerData);
    }

    async getCustomer(opts) {
        const uri = `https://crm.iqmetrix.net/v1/Companies(${this.creds.CompanyId})/CustomerFull(${opts.customerId})`
        return this._makeCall('GET', uri);
    }

    async searchCustomers(opts) {
        const uri = `https://crm.iqmetrix.net/v1/Companies(${this.creds.CompanyId})/CustomerSearch?$filter=Criteria  eq '(${opts.query})'`
        return this._makeCall('GET', uri);
    }

    async customerDailyTHC(opts){
        const uri = `https://dispensary.iqmetrix.net/v1/companies/{${this.creds.companyId}}/locations/{${this.creds.locationId}}/customer/{${opts.customerId}}/cannabispurchasesummary/today`;
        return this._makeCall('GET', uri)
    }

    
}

module.exports = CRM;