const CovaAPI = require('./cova');

class POS extends CovaAPI {

    constructor(creds) {
        super(creds)
    }

    async createOrder(opts){
        const uri = `https://api.iqmetrix.net/covaorderintake/v1/CovaOrder`;
        return this._makeCall('POST', uri, opts.orderData)
    }

    async changeOrderStatus(opts) {
        const uri = `https://pointofsale.iqmetrix.net/Companies(${this.creds.CompanyId})/OrderStatus(${opts.orderId})`;
        const data = { "Status": opts.status }
        return this._makeCall('PUT', uri, data);
    }

    async completeOrder(opts) {
        const uri = `https://pointofsale.iqmetrix.net/Companies(${this.creds.CompanyId})/Sales(${opts.orderId})/Complete`;
        return this._makeCall('POST', uri);
    }

    async cancelOrder(opts) {
        const uri = `https://pointofsale.iqmetrix.net/Companies(${this.creds.CompanyId})/Sales(${opts.orderId})`;
        return this._makeCall('DELETE', uri);
    }

    async acceptPayment(opts){
        const uri = `https://api.iqmetrix.net/covaorderintake/v1/CovaOrderPayment`;
        return this._makeCall('POST', uri, opts.paymentData)
    }

    async getOpenOrders(opts) {
        let uri = `https://pointofsale.iqmetrix.net/Companies(${this.creds.CompanyId})/Locations(${this.creds.LocationId})/SalesToFulfill`;

        if (opts) {

            if (opts.status || opts.method || opts.receiptNumber) {
                uri += `?$filter=`;
            }
            if (opts.receiptNumber) {
                uri += `ReceiptNumber eq '${opts.receiptNumber}'`;
                opts.status = false;
                opts.method = false;
            }

            if (opts.status) {
                uri += `Status eq '${opts.status}'`;
            }

            if (opts.status && opts.method) {
                uri += ` and `;
            }

            if (opts.method) {
                uri += `Method eq '${opts.method}'`;
            }
        }

        return this._makeCall('GET', uri)

    }

    async getOrder(opts) {
        const uri = `https://pointofsale.iqmetrix.net/Companies(${this.creds.CompanyId})/Sales(${opts.orderId})`
        return this._makeCall('GET', uri)
    }

    async getInvoice(opts) {
        let uri = '';
        if (opts.invoiceNumber) {
            uri += `https://invoicereporting.iqmetrix.net/v1/invoice/report?filter=companyId+eq+${this.creds.CompanyId}+and+entityId+eq+${this.creds.EntityId}+and+invoiceNo+eq+${opts.invoiceNumber}`
        }
        if (opts.receiptNumber) {
            uri += `https://pointofsale.iqmetrix.net/Companies(${this.creds.CompanyId})/Locations(${this.creds.LocationId})/SalesToFulfill?$filter=ReceiptNumber eq 'I${opts.receiptNumber}'`
        }
        return this._makeCall('GET', uri)
    }

    async getInvoiceByCustomerId(opts) {
        const uri = `https://pointofsale.iqmetrix.net/Companies(${this.creds.CompanyId})/SalesInvoiceSummaries?$filter=CustomerId eq guid'${opts.customerId}'`
        return this._makeCall('GET', uri)
    }
}

module.exports = POS;