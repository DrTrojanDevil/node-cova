# Node-Cova



Node-Cova is a `API-Wrapper` that allows async/await based standarized access to the COVA POS API.


## Prerequisites

Before you begin, ensure you have met the following requirements:
<!--- These are just example requirements. Add, duplicate or remove as required --->
* Have an API key and credentials from Cova
* create a JSON file with the credentials that you dont store with in cource control
```
{
    "grant_type" : "password",
    "username" : "your.COVA.user.id",
    "password" : "S0m3$ecR3tstR1n6",
    "client_id" : "some.client",
    "client_secret" : "fakeskf9834touherug9eighse",
    "access_token" : "",
    "CompanyId" : "12345",
    "EntityId" : "67890",
    "LocationId" : "45678"
}
```


## Installing node-cova

To install node-cova, follow these steps:


```
npm install node-cova --save
```

## Using node-cova

To use node-cova, follow these steps:

Inculde it in the file 
```
const COVA = require('node-cova')
const creds = require('/path/to/creds.json')
const cova = new COVA(creds);
```

* All methods are accessed through their class: either `cova.pos` or `cova.crm`
* All methods use `async/await` and therefore return promises
* All methods take 1 `opts` argument -- see below for method specific details

Examples: 
```
const opts = {customerId : someCustomerId}
const customer = await cova.crm.getCustomer(opts);
```

```
const opts = {customerId : someCustomerId}
cova.crm.getCustomer(opts).then( customer => {
    //do stuff with customer
    console.log(customer)
})
```
### POS

Methods:

* `createOrder`
```
const opts = {
    orderData : {
    "CompanyId": {{CompanyId}},
    "EntityId": {{EntityId}},
    "FirstName": '{{FirstName}}',
    "LastName": '{{LastName}}',
    "customerId": '{{CustomerId}}',
    "IsDelivery": 'true',
    "Items": [
        {
        "Name": 'weed',
        "CatalogItemId": '5a8bdb75-0bae-46a7-bb6f-27412475e0f3',
        "Quantity": 1,
        "SellAtPricePerUnit": 18
        }
    ]
    }
}

```
OR

```
const opts = {
    orderData : {
    "CompanyId": {{CompanyId}},
    "EntityId": {{EntityId}},
    "FirstName": '{{FirstName}}',
    "LastName": '{{LastName}}',
    "customerId": '{{CustomerId}}',
    "IsDelivery": '{{IsDelivery}}',
    "Items": [
        {
        "Name": '{{ItemName}}',
        "CatalogItemId": '{{CatalogItemId}}',
        "Quantity": 1,
        "SellAtPricePerUnit": 18
        }
    ],
    "Discount": {
        "InvoiceLevel": [
        {
            "Name": "Test",
            "Value": 1,
            "ValueType": "Amount"
        }
        ]
    },
    
    "Taxes": [
    {
    "TaxId": "55908b1e-9086-4964-bb1d-f0f305f77a3b",
    "TaxAmount": 1.11
    },
    {
    "TaxId": "b462b828-b801-46b3-9ba7-784f60f58693",
    "TaxAmount": 2.22
    },
    {
    "TaxId": "0ffc6dbe-3aff-4348-9cfc-e4ae4935ac53",
    "TaxAmount": 3.33
    }
    ]
    }
}

```
* `changeOrderStatus`
```
const opts = {
    status : 'InProgress' || 'ReadyForPickup',
    orderId : someOrderId
}
```

* `completeOrder`
```
const opts = {
    orderId : someOrderId
}
```

* `cancelOrder`
```
const opts = {
    orderId : someOrderId
}
```

* `acceptPayment`
```
const opts = {
    paymentData : {
    "SaleID": '{{SalesOrderId}}',
    "CompanyId": {{CompanyId}},
    "TrackingNumber": "TestingCompletionMethod",
    "Payments": [
        {
        "PreAuthorizationToken": "abc",
        "Amount": 22.52,
        "TimeTakenUtc": "2019-02-11T20:48:44.0946459Z"
        }
    ]
    }
}
```

* `getOpenOrders`
```
const opts = {
    receiptNumber : someReceiptNUmber
}
```
OR 

```
const opts = {
    status : 'OrderPlaced' || 'InProgress' || 'ReadyForPickup',
    method: 'InStore' || 'Pickup' || 'Delivery'
}
```

* `getOrder`
```
const opts = {
    orderId : someOrderId
}
```

* `getInvoice`
```
const opts = {
    invoiceId : someInvoiceId
}
```

OR

```
const opts = {
    receiptNumber : someReceiptNUmber
}
```

* `getInvoiceByCustomerId`
```
const opts = {
    customerId : someCustomerId
}

```
### CRM

* `getCustomer`
```
const opts = {
    customerId : someCustomerId
}
```

* `searchCustomers`
```
const opts = {
    query : someQueryString
}
```

* `createCustomer`
* `customerDailyTHC`

### Compliance

* `customerDailyTHC` (duplicate of CRM method for conveience)
* `getPurchasingLimitsByLocation`
* `getCompanyLicenses`
* `getSupplierLicenses`


## Changelog
* 1.1 Added Compliance module and CreateUser and UserTHC to the CRM module; Fixed a bug where POS.getOpenOrders with a `Method` query would fail
* 1.1.1 Fixed missing module

## Contributing to node-cova
<!--- If your README is long or you have some specific process or steps you want contributors to follow, consider creating a separate CONTRIBUTING.md file--->
To contribute to node-cova, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin node-cova/<location>`
5. Create the pull request.

Alternatively see the GitHub documentation on [creating a pull request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## Contributors

Thanks to the following people who have contributed to this project:

* [@DrTrojanDevil](https://gitlab.com/drtrojandevil) 📖



## Contact

If you want to contact me you can reach me at aaron@lucidsystems.io.

